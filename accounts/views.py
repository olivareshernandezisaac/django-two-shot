from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate,logout
from django.contrib.auth.models import User
from accounts.form import AccountForm, SignUpForm

# Create your views here.
def user_login(request):
    if request.method == "POST": #POST Method when user clicks Login in the AccountForm
        form = AccountForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None: #f you have an authenticated user you want to attach to the current session - this is done with a login() function.
                login(request, user) #the http request  is used by the login function to associte the user with the current session, and User is the is an instance of django 'User' model that inherits from 'AbstractBaseUser'
                return redirect("home") #login function uses this user object to authenticate and associate the user with the current session
    else: #GET METHOD
        form = AccountForm()
    context ={
        "form_account" : form,
    }
    return render(request, "accounts/login.html", context)

def user_logout(request):
    logout(request) #takes an httprequest object and has not return value, which login funcion does
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        form_signup = SignUpForm(request.POST) #populates the form from the POST request
        if form_signup.is_valid():#checks that the form is valid
            username = form_signup.cleaned_data["username"] #retrieves cleaned data from the form_signup
            password = form_signup.cleaned_data["password"] #creates a passowd
            password_confirmation = form_signup.cleaned_data["password_confirmation"]

            if password == password_confirmation: #checks that both the same
                user = User.objects.create_user(    #at this point we have a new User object in the User Class,
                    username,                       #now the new User object  saved to the database.
                    password=password, #explicitly assign the value of the password variable to itself
                )
                login(request, user)
                return redirect("home")
            else:
                form_signup.add_error("password","the passwords do not match")
    else:
        form_signup = SignUpForm()
    context = {
        "form_signup": form_signup
    }
    return render(request, "accounts/signup.html",context)


#to update password you can use PassworChange From. Look at django bookmark "how to update password"
