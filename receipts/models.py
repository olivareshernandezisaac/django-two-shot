from django.db import models
from django.conf import settings

# Create your models here.
class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50) #type of expense like gas, entertainment, music , etc

    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Account(models.Model):
    name = models.CharField(max_length=100) #name of credit card like business, freedom, etc
    number = models.CharField(max_length=20)#credit card number

    owner = models.ForeignKey(  #owner of that card many accounts one owner relationship
        settings.AUTH_USER_MODEL,
        related_name = "accounts",
        on_delete = models.CASCADE,
    )

    def __str__(self):
        return self.name


class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateTimeField(null=True)

    purchaser = models.ForeignKey( #will be the owner who made the transaction
        settings.AUTH_USER_MODEL,
        related_name ="receipts",
        on_delete=models.CASCADE,
    )

    category = models.ForeignKey(        # Expense category has many receipts. One to many relationship
        ExpenseCategory,                #by using category property we can acccess the name in Associated Category class. Ex in html. receipt.category.name accesses the name of the category of that receipt
        related_name = "receipts",
        on_delete=models.CASCADE,
    )

    account = models.ForeignKey(     #one account many receipts
        Account,
        related_name ="receipts",
        on_delete=models.CASCADE,
        null = True
    )

# we use the referencing related_name in the foreign key in Receipt Model
#for category_list


# we create the models here to
# allow us to communnicate with the server
# with the template and with the forms
