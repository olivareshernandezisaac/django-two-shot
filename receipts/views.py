from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
# Create your views here.

@login_required
def receipt_list_list(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user) #filters and only gets the receipts of the logged in user is the same as the purchaser(attribute in Receipt object)
    context = {                                                  #we are going to filter all of the receipts for that specific purchaser
        "receipt_list": receipt_list,
    }
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form_create = ReceiptForm(request.POST)
        if form_create.is_valid():
            receipt = form_create.save(False)
            receipt.purchaser = request.user    #the purchaser is set to the current user
            receipt.save()
            return redirect("home")
    else:
        form_create = ReceiptForm()

    context = {
        "form_c": form_create,
    }
    return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user) #will make sure that the logged in user matches the owner(attribute in ExpenseCategory)
    context = {
        "categories_list": categories,
    }
    return render(request, "categories/categories.html", context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "account_list": accounts,
    }
    return render(request, "accounts/accounts.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form_category = ExpenseCategoryForm(request.POST)
        if form_category.is_valid():
            category = form_category.save(False)
            category.owner = request.user       #associates the owner with the category and s
            category.save()                     #saves the form under that owner in the database.
            return redirect("category_list")
    else:
        form_category = ExpenseCategoryForm()
    context = {
        "form_category": form_category,
    }
    return render(request, "categories/create.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form_account = AccountForm(request.POST)
        if form_account.is_valid():
            account = form_account.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form_account = AccountForm()
    context = {
        "form_account": form_account,
    }
    return render(request, "accounts/create.html", context)


# We create the views here from the models in models.py
