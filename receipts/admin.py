from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
        "id",
    )

@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "category",
        "account",
        "id",
    )



#We come here after after creating models in models.py
#registering them in admin will allow us to see them onlin in the admin page
#admin site online...on localhost8000 website
