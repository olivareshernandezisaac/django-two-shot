"""expenses URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect
from django.views.generic.base import RedirectView

def redirect_to_receipt_list_list(request): #found in receipts view
     if request.method == "GET":
         return redirect("home")    #it comes from urls in receipts, it used the name = "home"


urlpatterns = [
    path("admin/", admin.site.urls),
    path("receipts/", include("receipts.urls")),
    # could use this as well to redirect without the function, path("", RedirectView.as_view(url='/receipts/'), name="receipts"),
    path("", redirect_to_receipt_list_list, name="receipts"),
    path("accounts/", include("accounts.urls")),

]
